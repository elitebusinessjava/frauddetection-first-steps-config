package com.elite.cdr.validator;

import com.beust.jcommander.JCommander;
import com.elite.cdr.validator.Asn1Classes.CallDetailRecord;
import com.elite.cdr.validator.Asn1Classes.CallDetailRecord2;
import com.elite.cdr.validator.Asn1Classes.RawFileAsBinaryInputFormat;
import com.elite.cdr.validator.utils.Settings;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import scala.Tuple2;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

import static com.elite.cdr.validator.utils.Settings.LOCAL;

public class Application {
    private static final Logger LOGGER =
            Logger.getLogger(Application.class);

    private static final String LOGGER_SEPARATOR =
            "****************************************************";

    public static void main(String[] args) {

        LOGGER.info("Starting ASN1 Reader ");

        LOGGER.info("############### Run with the args " + Arrays.toString(args));

        Settings settings = new Settings();

        try {
            JCommander.newBuilder().addObject(settings).build().parse(args);
            LOGGER.info("############### Run in " + settings.getRunningEnv() + " mode");

        } catch (Exception e) {
            LOGGER.error(e);
            System.exit(1);
        }

        if (LOCAL.equals(settings.getRunningEnv())) {
            setHadoopHome();
        }

        SparkConf conf = new SparkConf().setAppName("Feedback Analyzer").setMaster("local[*]");
        SparkSession spark = SparkSession.builder().config(conf).getOrCreate();
        //SparkContext spark = new SparkContext(conf);
    /*Dataset<Row> ds = spark.read()//.format("asn1")
           .format("csv").option("header","true").option("delimiter", ";")
           .load(settings.getFilePath());
    ds.show();
    /**********/
        //spark.bi
        Dataset<Row> asn1DataFrame = spark.read().format("binaryFile")
                .load(settings.getFilePath());
                //.option("schemaFileType", "asn")
                //.option("schemaFilePath", "src/main/resources/data/simpleTypes.asn")
                //.load("src/main/resources/data/simpleTypes.ber");
        asn1DataFrame.printSchema();
        asn1DataFrame.show();

        final long begin = System.currentTimeMillis();
        logSuccessMessage(begin);

    }

    private static void logSuccessMessage(long begin) {
        final long endTime = System.currentTimeMillis();
        final long elapsedTime = (endTime - begin);

        LOGGER.info(LOGGER_SEPARATOR);
        LOGGER.info("Duration: " + elapsedTime / 1000d + " seconds");
        LOGGER.info("Batch executed with success");
        LOGGER.info(LOGGER_SEPARATOR);
    }

    private static void logErrorMessage(long begin, Exception exception) {
        final long end = System.currentTimeMillis();
        final long elapsedTime = (end - begin);

        LOGGER.error(LOGGER_SEPARATOR);
        LOGGER.error("Duration: " + elapsedTime / 1000d + " seconds");
        LOGGER.error("Error occurred while executing treatment");
        LOGGER.error("Check stacktrace for more information", exception);
        LOGGER.error(LOGGER_SEPARATOR);
    }

    public static void setHadoopHome() {
        final URL resources =
                Application.class.getClassLoader().getResource("./");
        try {
            System.setProperty("hadoop.home.dir", resources.toURI().getPath());
        } catch (URISyntaxException e) {
            LOGGER.warn("Unable to setup hadoop.home.dir for winutils, continuing ...");
        }
    }
}
