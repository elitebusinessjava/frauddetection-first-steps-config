# FraudDetection

In this project we will implement a spark streaming reader
for files flow on a directory.
Then we will decode these files with custom decoding. 
Next step, will be loading them as tables.

Args for the project.
Environment and file path for reading the csv.
###Example:
--env
local
--file
C:\Users\Bold\projects\FraudDetection\src\main\resources\data\username.csv
